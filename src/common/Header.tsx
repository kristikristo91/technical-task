/*
 * File: Header.tsx
 * Project: test-assignment
 * File Created: Saturday, 18th May 2019 10:47:58 pm
 * Author: Kristi Kristo (kristikristo91@gmail.com)
 * -----
 * Last Modified: Saturday, 18th May 2019 10:48:10 pm
 * Modified By: Kristi Kristo (kristikristo91@gmail.com>)
 */

import * as React from 'react';
import Configuration from 'src/configuration/config.json';

const Header = () => (
  <section className="hero is-primary">
    <div className="hero-body">
      <div className="container">
        <h1 className="title">
          React Application
        </h1>
        <h1 className="subtitle">
          This is a test task on <strong>{Configuration.environment}</strong> environment
        </h1>
      </div>
    </div>
  </section>
);

export default Header;
