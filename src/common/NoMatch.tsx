/*
 * File: Header.tsx
 * Project: test-assignment
 * File Created: Saturday, 18th May 2019 10:47:58 pm
 * Author: Kristi Kristo (kristikristo91@gmail.com)
 * -----
 * Last Modified: Saturday, 18th May 2019 10:48:10 pm
 * Modified By: Kristi Kristo (kristikristo91@gmail.com>)
 */

import * as React from 'react';

const NoMatch = () => (
  <div>Route is wrong</div>
);

export default NoMatch;
