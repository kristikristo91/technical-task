import * as React from 'react';
import Header from 'src/common/Header';
import ApplicationRoutes from 'src/routes';

class App extends React.Component {
  public render() {
    return (
      <div>
        <Header />
        <ApplicationRoutes />
      </div>
    );
  }
}

export default App;
