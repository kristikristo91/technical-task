/*
 * File: api.ts
 * Project: test-assignment
 * File Created: Sunday, 19th May 2019 7:13:17 pm
 * Author: Kristi Kristo (kristikristo91@gmail.com)
 * -----
 * Last Modified: Sunday, 19th May 2019 7:14:36 pm
 * Modified By: Kristi Kristo (kristikristo91@gmail.com>)
 */


class APIManager {
  /**
   * Calculate results based on the data
   */
  public calculateResults = async (data: any) => {
    // Apply business logic based on data, but for this assignment I skipped it
    // for the simple reason that I'm really not sure how those variables can
    // help into calculating anything relevant.
    await this.timeout(2000); // Let's wait 2 seconds before a reply -> Faking API call
    return Math.round(Math.random() * 100);
  }

  /**
   * Delay functionality
   */
  private timeout = (ms: number) => new Promise(res => setTimeout(res, ms))
}

export default new APIManager();