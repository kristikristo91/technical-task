/*
 * File: routes.tsx
 * Project: test-assignment
 * File Created: Saturday, 18th May 2019 10:26:46 pm
 * Author: Kristi Kristo (kristikristo91@gmail.com)
 * -----
 * Last Modified: Saturday, 18th May 2019 10:46:47 pm
 * Modified By: Kristi Kristo (kristikristo91@gmail.com>)
 */

import * as React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import LandingScreen from 'src/screens/Landing';
import QuestionaireScreen from 'src/screens/Questionaire';

const RouterHandler = () => (
  <Router>
    <Switch>
      <Route exact={true} path="/" component={LandingScreen} />
      <Route path="/questionaire/:gender" component={QuestionaireScreen} />
      <Route component={NoMatch} />
    </Switch>
  </Router>
);

/**
 * No Match Component
 */
const NoMatch = () => (
  <div>Route is wrong</div>
);

export default RouterHandler;
