/*
 * File: index.tsx
 * Project: test-assignment
 * File Created: Saturday, 18th May 2019 10:31:35 pm
 * Author: Kristi Kristo (kristikristo91@gmail.com)
 * -----
 * Last Modified: Saturday, 18th May 2019 10:47:38 pm
 * Modified By: Kristi Kristo (kristikristo91@gmail.com>)
 */
import View from './View';

export default View;