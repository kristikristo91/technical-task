/*
 * File: View.tsx
 * Project: test-assignment
 * File Created: Saturday, 18th May 2019 10:31:39 pm
 * Author: Kristi Kristo (kristikristo91@gmail.com)
 * -----
 * Last Modified: Saturday, 18th May 2019 10:47:34 pm
 * Modified By: Kristi Kristo (kristikristo91@gmail.com>)
 */
import * as React from 'react';
import { Link } from 'react-router-dom';
import './style.css';

enum IGender {
  Male = "Male",
  Female = "Female"
}

interface IContentBoxProps {
  gender: IGender;
}

const ContentBox = ({ gender }: IContentBoxProps) => (
  <Link className="column" to={`/questionaire/${gender === IGender.Male ? 'man' : 'woman'}`}>
    <div className="list-group-item">
      <img src={`http://kamelrechner.eu/img/select-${gender === IGender.Male ? 'man' : 'woman'}.png`} alt="Image" />
      <span>
        How many camels for your {gender === IGender.Male ? 'boyfriend' : 'girlfriend'}?
      </span>
    </div>
  </Link>
);

const LandingView = () => (
  <section className="section">
    <div className="container">
      <h2 className="subtitle">
        Here you can calculate how many camels your girlfriend or boyfriend is worth. But first we need some information about the person.
          </h2>

      <div className="columns">
        <ContentBox gender={IGender.Female} />
        <ContentBox gender={IGender.Male} />
      </div>
      <hr />
    </div>
  </section>  
);

export default LandingView;
