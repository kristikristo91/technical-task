/*
 * File: ResultView.tsx
 * Project: test-assignment
 * File Created: Sunday, 19th May 2019 8:16:23 pm
 * Author: Kristi Kristo (kristikristo91@gmail.com)
 * -----
 * Last Modified: Sunday, 19th May 2019 8:16:31 pm
 * Modified By: Kristi Kristo (kristikristo91@gmail.com>)
 */

import * as React from 'react';
import AnimatedNumber from 'react-animated-number';
import { FacebookProvider, Share } from 'react-facebook';
import { Link } from 'react-router-dom';
import Configuration from 'src/configuration/config.json';

const Results = ({ result, match}: any) => (
  <section className="section">
    <div className="container has-text-centered">
      <h4 className="subtitle">Your {match.params.gender} is worth</h4>
      <h1 className="title">
        <AnimatedNumber
          component="text"
          value={result}
          style={{
            fontSize: 48,
            transition: '0.8s ease-out',
            transitionProperty:
              'background-color, color, opacity'
          }}
          duration={1500}
          formatValue={(n: number) => Math.round(n)}
        />
      </h1>
      <h4 className="subtitle">camels</h4>
    </div>
    <div className="has-text-centered">
      <Link className="column" to="/">
        <span className="button is-primary">Restart Process</span>
      </Link>
      <FacebookProvider appId={Configuration.fbId}>
        <Share href="www.softup.co">
          {({ handleClick, loading }: any) => (
            <button type="button" disabled={loading} onClick={handleClick}>Share on FB</button>
          )}
        </Share>
      </FacebookProvider>
    </div>
  </section>
);

export default Results;