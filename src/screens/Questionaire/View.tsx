/*
 * File: View.tsx
 * Project: test-assignment
 * File Created: Saturday, 18th May 2019 10:30:51 pm
 * Author: Kristi Kristo (kristikristo91@gmail.com)
 * -----
 * Last Modified: Saturday, 18th May 2019 10:47:15 pm
 * Modified By: Kristi Kristo (kristikristo91@gmail.com>)
 */

import { ErrorMessage, Field, Form, Formik, FormikActions } from 'formik';
import React, { useState } from 'react';
import { RouteComponentProps } from 'react-router-dom';

import APIManager from 'src/services/api';
import ResultsView from './ResultView';
import { QuestionaireSchema } from './validators';

import './style.css';

interface IQuestionaireMatchProps {
  gender: string;
}

interface IQuestionaireProps extends RouteComponentProps<IQuestionaireMatchProps> {
}

interface IFormValues {
  age: number;
  height: number;
  hairColour: string;
}

const Fieldset = ({ name, ...rest }: any) => (
  <React.Fragment>
    <Field id={name} name={name} {...rest} />
  </React.Fragment>
);

const QuestionaireScreen = (props: IQuestionaireProps & IQuestionaireMatchProps) => {
  const [result, setResult] = useState(0);

  if (result) {
    return <ResultsView result={result} {...props} />
  }

  return (
    <section className="section">
      <div className="container has-text-centered">
        <h2 className="title">Please fill up the questionaire</h2>
        <hr />
        <Formik
          initialValues={{
            age: 24,
            hairColour: '',
            height: 180,
          }}
          validationSchema={QuestionaireSchema}
          onSubmit={async (values: IFormValues, { setSubmitting }: FormikActions<IFormValues>) => {
            const results = await APIManager.calculateResults(values);
            setResult(results);
            setSubmitting(false);
          }}
          render={({ values, isSubmitting }) => (
            <Form>
              <div className="columns">
                <div className="column">
                  <h4 className="title is-4">Age</h4>
                </div>
                <div className="column">
                  <h6 className="title is-6">{values.age}</h6>
                  <Field id="age" type="range" min="14" max="70" value={values.age} />
                </div>
              </div>
              <div className="columns">
                <div className="column">
                  <h4 className="title is-4">Height (cm)</h4>
                </div>
                <div className="column">
                  <h6 className="title is-6">{values.height}</h6>
                  <Field id="height" type="range" min="140" max="220" value={values.height} />
                </div>
              </div>
              <div className="columns">
                <div className="column">
                  <h4 className="title is-4">Hair Colour</h4>
                </div>
                <div className="column">
                  <div className="select">
                    <Fieldset name="hairColour" component="select">
                      <option value="">Select a colour</option>
                      <option value="blonde">Blonde</option>
                      <option value="brown">Brown</option>
                      <option value="black">Black</option>
                    </Fieldset>
                    <ErrorMessage
                      name="hairColour"
                      component="div"
                      className="field-error"
                    />
                  </div>
                </div>
              </div>
              <hr />
              <div className="control has-text-centered">
                <button type="submit" className={`button is-link ${isSubmitting && 'is-loading'}`}>Submit</button>
              </div>
            </Form>
          )}
        />
      </div>
    </section>
  );
}

export default QuestionaireScreen;
