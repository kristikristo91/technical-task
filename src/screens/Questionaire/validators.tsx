/*
 * File: validators.tsx
 * Project: test-assignment
 * File Created: Sunday, 19th May 2019 7:10:32 pm
 * Author: Kristi Kristo (kristikristo91@gmail.com)
 * -----
 * Last Modified: Sunday, 19th May 2019 7:10:53 pm
 * Modified By: Kristi Kristo (kristikristo91@gmail.com>)
 */
import * as Yup from 'yup';

/**
 * Validate Questionaire Schema
 */
export const QuestionaireSchema = Yup.object().shape({
  age: Yup.number()
    .required('Please fill up height'),
  hairColour: Yup.string()
    .oneOf(['blonde', 'brown', 'black'])
    .required('Hair Colour is required'),
  height: Yup.number()
    .required('Please fill up height'),
});
