/*
 * File: index.tsx
 * Project: test-assignment
 * File Created: Saturday, 18th May 2019 10:30:46 pm
 * Author: Kristi Kristo (kristikristo91@gmail.com)
 * -----
 * Last Modified: Saturday, 18th May 2019 10:47:23 pm
 * Modified By: Kristi Kristo (kristikristo91@gmail.com>)
 */
import View from './View';

export default View;