# Technical Task

### List of tasks
- ~~Boilerplate setup (router, screens etc)~~
- ~~Webpack is already configured for zipped/minified/uglified on production~~
- ~~Different environments are handled with configuration file, for development, staging and production~~
- ~~Bulma is added as CDN~~
- ~~Linting & compiling is enforced~~
- ~~Implement screens~~
- ~~Implement business logic~~
- ~~FB Share done~~
- ~~Webpack zipping functionality, but without any upload or whatever...~~~

PS Did setup the game, but didn't have time to deploy / add it in FB Instant Games hosting platform. Instead, created infrastructure in AWS with Route53 + CDN + S3 and deployed via CircleCI pipeline -> https://technical-task.softup.co