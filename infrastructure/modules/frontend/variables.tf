variable "domain_name" {
  description = "Name of the S3 bucket"
}

variable "acm_certificate_arn" {
  description = "ARN for SSL certificate"
}

variable "application_name" {
  description = "Name of the application"
}
variable "route_zone_id" {
  description = "Zone ID where the application should be added"
}
