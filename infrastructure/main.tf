provider "aws" {
  access_key = ""
  secret_key = ""
  region     = "us-east-1"
}

variable "root_domain" {
  default = "softup.co"
}

variable "technical_task_domain" {
  default = "technical-task.softup.co"
}

data "aws_acm_certificate" "ssl_cert" {
  domain   = "*.softup.co"
  statuses = ["ISSUED"]
  most_recent = true
}

data "aws_route53_zone" "technical_task_zone" {
  name = "softup.co."
}


module "technical_task_application" {
  application_name    = "technical-task"
  source              = "./modules/frontend"
  domain_name         = "${var.technical_task_domain}"
  acm_certificate_arn = "${data.aws_acm_certificate.ssl_cert.arn}"
  route_zone_id       = "${data.aws_route53_zone.technical_task_zone.zone_id}"
}
